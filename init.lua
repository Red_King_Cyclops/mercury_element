--Solid Nodes

minetest.register_node("mercury_element:stone_with_cinnabar", {
    description = "Cinnabar Ore",
    tiles = {"default_stone.png^mercury_element_mineral_cinnabar.png"},
    is_ground_content = true,
    drop = "mercury_element:cinnabar_gem 1",
    groups = {cracky=3}
})

minetest.register_node("mercury_element:cinnabar_block", {
    description = "Cinnabar Block",
    tiles = {"mercury_element_cinnabar_block.png"},
    is_ground_content = false,
    groups = {cracky=3}
})

minetest.register_node("mercury_element:chiseled_cinnabar", {
    description = "Chiseled Cinnabar",
    tiles = {"mercury_element_chiseled_cinnabar.png"},
    is_ground_content = false,
    groups = {cracky=3}
})

stairs.register_stair_and_slab(
	"cinnabar",
	"mercury_element:cinnabar_block",
	{cracky=3},
	{"mercury_element_cinnabar_block.png"},
	"Cinnabar Block Stair",
	"Cinnabar Block Slab"
	--default.node_sound_wood_defaults()
)

--Liquid Nodes

minetest.register_node("mercury_element:mercury_source", {
	description = "Mercury Source",
	drawtype = "liquid",
	tiles = {
		{
			name = "mercury_element_mercury_source_animated.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 2.0,
			},
		},
	},
	special_tiles = {
		{
			name = "mercury_element_mercury_source_animated.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 2.0,
			},
			backface_culling = false,
		},
	},
	paramtype = "light",
	walkable = false,
	pointable = false,
	diggable = false,
	buildable_to = true,
	is_ground_content = false,
	drop = "",
	drowning = 1,
	liquidtype = "source",
	liquid_alternative_flowing = "mercury_element:mercury_flowing",
	liquid_alternative_source = "mercury_element:mercury_source",
	liquid_viscosity = 1,
    liquid_renewable = false,
    damage_per_second = 4 * 2,
	post_effect_color = {a = 191, r = 219, g = 206, b = 202},
	groups = {liquid = 3, puts_out_fire = 1},
	--sounds = default.node_sound_water_defaults(),
})

minetest.register_node("mercury_element:mercury_flowing", {
	description = "Flowing Mercury",
	drawtype = "flowingliquid",
	tiles = {"mercury_element_mercury.png"},
	special_tiles = {
		{
			name = "mercury_element_mercury_flowing_animated.png",
			backface_culling = false,
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 0.8,
			},
		},
		{
			name = "mercury_element_mercury_flowing_animated.png",
			backface_culling = true,
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 0.8,
			},
		},
	},
	paramtype = "light",
	paramtype2 = "flowingliquid",
	walkable = false,
	pointable = false,
	diggable = false,
	buildable_to = true,
	is_ground_content = false,
	drop = "",
	drowning = 1,
	liquidtype = "flowing",
	liquid_alternative_flowing = "mercury_element:mercury_flowing",
	liquid_alternative_source = "mercury_element:mercury_source",
	liquid_viscosity = 1,
	liquid_renewable = false,
	damage_per_second = 4 * 2,
	post_effect_color = {a = 191, r = 219, g = 206, b = 202},
	groups = {liquid = 3, puts_out_fire = 1,
		not_in_creative_inventory = 1},
	--sounds = default.node_sound_water_defaults(),
})

--Items

minetest.register_craftitem("mercury_element:cinnabar_gem", {
    description = "Cinnabar Gem",
    inventory_image = "mercury_element_cinnabar_gem.png"
})

minetest.register_craftitem("mercury_element:cinnabar_powder", {
    description = "Cinnabar Powder",
    inventory_image = "mercury_element_cinnabar_powder.png"
})

minetest.register_craftitem("mercury_element:bucket_cinnabar_powder", {
    description = "Cinnabar Powder in a Bucket",
    inventory_image = "mercury_element_bucket_cinnabar_powder.png",
    stack_max = 1
})

bucket.register_liquid(
	"mercury_element:mercury_source",
	"mercury_element:mercury_flowing",
	"mercury_element:bucket_mercury",
	"mercury_element_bucket_mercury.png",
	"Mercury Bucket"
)

--Ores

    --Cinnabar ore

	minetest.register_ore({
		ore_type       = "scatter",
		ore            = "mercury_element:stone_with_cinnabar",
		wherein        = "default:stone",
		clust_scarcity = 14 * 14 * 14,
		clust_num_ores = 5,
		clust_size     = 3,
		y_min          = 1025,
		y_max          = 31000,
	})

	minetest.register_ore({
		ore_type       = "scatter",
		ore            = "mercury_element:stone_with_cinnabar",
		wherein        = "default:stone",
		clust_scarcity = 18 * 18 * 18,
		clust_num_ores = 3,
		clust_size     = 2,
		y_min          = -255,
		y_max          = -64,
	})

	minetest.register_ore({
		ore_type       = "scatter",
		ore            = "mercury_element:stone_with_cinnabar",
		wherein        = "default:stone",
		clust_scarcity = 14 * 14 * 14,
		clust_num_ores = 5,
		clust_size     = 3,
		y_min          = -31000,
		y_max          = -256,
	})

--Crafting Recipes

minetest.register_craft({
    type = "shaped",
    output = "mercury_element:cinnabar_block 1",
    recipe = {
        {"mercury_element:cinnabar_gem", "mercury_element:cinnabar_gem", "mercury_element:cinnabar_gem"},
        {"mercury_element:cinnabar_gem", "mercury_element:cinnabar_gem", "mercury_element:cinnabar_gem"},
        {"mercury_element:cinnabar_gem", "mercury_element:cinnabar_gem", "mercury_element:cinnabar_gem"}
    }
})

minetest.register_craft({
    type = "shapeless",
    output = "mercury_element:cinnabar_gem 9",
    recipe = {
        "mercury_element:cinnabar_block"
    }
})

minetest.register_craft({
    type = "shapeless",
    output = "mercury_element:cinnabar_powder 9",
    recipe = {
        "mercury_element:cinnabar_gem"
    }
})

minetest.register_craft({
    type = "shaped",
    output = "mercury_element:cinnabar_gem 1",
    recipe = {
        {"mercury_element:cinnabar_powder", "mercury_element:cinnabar_powder", "mercury_element:cinnabar_powder"},
        {"mercury_element:cinnabar_powder", "mercury_element:cinnabar_powder", "mercury_element:cinnabar_powder"},
        {"mercury_element:cinnabar_powder", "mercury_element:cinnabar_powder", "mercury_element:cinnabar_powder"}
    }
})

minetest.register_craft({
    type = "shapeless",
    output = "mercury_element:bucket_cinnabar_powder",
    recipe = {
        "mercury_element:cinnabar_powder",
        "bucket:bucket_empty"
    }
})

minetest.register_craft({
    type = "cooking",
    output = "mercury_element:bucket_mercury",
    recipe = "mercury_element:bucket_cinnabar_powder",
    --cooktime = 10,
})

minetest.register_craft({
    type = "shapeless",
    output = "mercury_element:chiseled_cinnabar",
    recipe = {
        "stairs:slab_cinnabar",
        "stairs:slab_cinnabar"
    }
})

