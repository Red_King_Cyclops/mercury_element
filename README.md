# mercury_element
### The mercury and cinnabar mod

This mod adds a new ore called cinnabar that can be made into a new deadly liquid called mercury.

Underground, cinnabar ore can be found at the same rarity as mese. The ore has cinnabar crystals coming out of dolomite. Mining cinnabar ore drops one cinnabar gem. A cinnabar gem can be crushed into cinnabar powder (just put one gem in the crafting table to get 9 powders) or crafted into a cinnabar block (fill the crafting table with gems to get 1 block). Cinnabar powder can be crafted with an empty bucket to make a bucket full of cinnabar powder. Cook this bucket to get a bucket of mercury. Buckets work on mercury just like water and lava. Mercury is a very dangerous liquid -- it causes as much damage as lava and flows as fast as water. A cinnabar block can be crafted into stairs and slabs like usual. Two cinnabar slabs can be crafted into a chiseled cinnabar block, which has a dragon carved on it.

The color for cinnabar is #E34234 (R: 227, G: 66, B: 52) and the color for mercury is #DBCECA (R: 219, G: 206, B: 202).

The code is licensed as LGPL 2.1 and the media as CC-BY-SA 3.0. The following textures are modified textures taken from default and bucket (both mods were made by the Minetest development team):
* mercury_element_bucket_cinnabar_powder.png
* mercury_element_bucket_mercury.png
* mercury_element_mercury.png
* mercury_element_mercury_flowing_animated.png
* mercury_element_mercury_source_animated.png

The mod depends on default, bucket, and stairs.